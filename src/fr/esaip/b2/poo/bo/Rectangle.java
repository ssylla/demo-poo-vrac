package fr.esaip.b2.poo.bo;

public class Rectangle extends Figure {
	
	private double largeur;
	private double longueur;
	
	public Rectangle( String nom, String couleur, double largeur, double longueur ) {
		super( nom, couleur );
		this.largeur = largeur;
		this.longueur = longueur;
	}
	
	@Override
	public double calculSurface() {
		return largeur * longueur;
	}
	
	@Override
	public double calculPerimetre() {
		return 2*(longueur+largeur);
	}
	
	public double getLargeur() {
		return largeur;
	}
	
	public void setLargeur( double largeur ) {
		this.largeur = largeur;
	}
	
	public double getLongueur() {
		return longueur;
	}
	
	public void setLongueur( double longueur ) {
		this.longueur = longueur;
	}
}
