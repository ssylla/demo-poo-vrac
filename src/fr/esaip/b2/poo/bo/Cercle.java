package fr.esaip.b2.poo.bo;

public class Cercle extends Figure {
	
	
	private double rayon;
	
	public Cercle(String nom, String couleur, double rayon ) {
		super(nom, couleur);
		this.rayon = rayon;
	}
	
	@Override
	public double calculSurface() {
		return Math.PI * Math.pow( rayon, 2 );
	}
	
	@Override
	public double calculPerimetre() {
		return 2 * Math.PI * rayon;
	}
	
	public double getRayon() {
		return rayon;
	}
	
	public void setRayon( double rayon ) {
		this.rayon = rayon;
	}
}
