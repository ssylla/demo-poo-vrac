package fr.esaip.b2.poo.birds;

public interface ICanFly {
	
	void fly();
}
