package fr.esaip.b2.poo.bo;

public class Triangle extends Figure {
	
	public Triangle( String nom, String couleur ) {
		super( nom, couleur );
	}
	
	@Override
	public double calculSurface() {
		return 0;
	}
	
	@Override
	public double calculPerimetre() {
		return 0;
	}
}
