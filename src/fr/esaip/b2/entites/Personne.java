package fr.esaip.b2.entites;

import java.util.Locale;

public class Personne {
	
	private String nom;
	String prenom;
	
	public Personne() {
	}
	
	public Personne( String nom ) {
		this.nom = nom;
	}
	
	public Personne( String nom, String prenom ) {
		this.nom = nom;
		this.prenom = prenom;
	}
	
	public void setNom(String nom) {
		this.nom = nom.toLowerCase( Locale.ROOT );
	}
	
	@Override
	public final String toString() {
		final StringBuilder sb = new StringBuilder( "Personne{" );
		sb.append( "nom='" ).append( nom ).append( '\'' );
		sb.append( ", prenom='" ).append( prenom ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
