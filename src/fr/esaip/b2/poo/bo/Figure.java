package fr.esaip.b2.poo.bo;

import java.util.Arrays;

public abstract class Figure {
	
	protected String nom;
	protected String couleur;
	private int[] tab;
	private static int[] tabStatic;
	
	//Bloc d'initialisation static exécuté au chargement de la classe par la JVM
	static {
		System.out.println("Chargement de la classe Figure ");
		tabStatic = new int[10];
		Arrays.fill(tabStatic, -1);
	}
	
	//Bloc d'initialisation d'instance utilisable pour factoriser les intructions
	//lors des créations d'objets de type figure
	//Ce bloc est exécuté à chaque new
	{
		System.out.println("appel un new");
		tab = new int[10];
		Arrays.fill(tab, -1);
	}
	
	public Figure( String nom, String couleur ) {
		this.nom = nom;
		this.couleur = couleur;
	}
	
	public abstract double calculSurface();
	
	public abstract double calculPerimetre();
	
	public String getNom() {
		return nom;
	}
	
	public void setNom( String nom ) {
		this.nom = nom;
	}
	
	public String getCouleur() {
		return couleur;
	}
	
	public void setCouleur( String couleur ) {
		this.couleur = couleur;
	}
}
