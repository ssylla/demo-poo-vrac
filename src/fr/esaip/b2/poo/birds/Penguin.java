package fr.esaip.b2.poo.birds;

public class Penguin extends Bird implements ICanSwim {
	public Penguin( String name ) {
		super( name );
	}
	
	@Override
	public void swim() {
		System.out.println("I swim like a penguin");
	}
}
