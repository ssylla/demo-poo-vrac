package fr.esaip.b2.poo.birds;

public class Dove extends Bird implements ICanFly {
	
	public Dove( String name ) {
		super( name );
	}
	
	@Override
	public void fly() {
		System.out.println("I fly like a dove");
	}
}
