package fr.esaip.b2.entites;

public class Apprenant extends Personne{
	
	protected String niveau;
	
	public Apprenant( String nom, String niveau ) {
		
		this.niveau = niveau;
	}
	
	public Apprenant( String nom, String prenom, String niveau ) {
		super( nom, prenom );
	}
}
