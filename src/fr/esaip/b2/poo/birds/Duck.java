package fr.esaip.b2.poo.birds;

public class Duck extends Bird implements ICanSwim, ICanFly{
	public Duck( String name ) {
		super( name );
	}
	
	@Override
	public void fly() {
		System.out.println("I fly like Donald");
	}
	
	@Override
	public void swim() {
		System.out.println("I swim like Donald");
	}
}
