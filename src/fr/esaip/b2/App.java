package fr.esaip.b2;

import fr.esaip.b2.entites.Personne;

import java.util.Locale;

public class App {
	
	public static void main( String[] args ) {
		
		Personne p = new Personne("Sega", "Sylla");
		p.setNom(  "test");
		
		m(p);
		
		
		System.out.println(p);
	}
	
	private static void m( Personne personne ) {
		
		Personne pers = personne;
		pers.setNom( "toto" );
		
		//TODO dispose pers;
	}
}
