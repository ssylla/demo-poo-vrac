package fr.esaip.b2.poo.birds;

public interface ICanSwim {
	
	void swim();
}
