package fr.esaip.b2.poo;

import fr.esaip.b2.poo.birds.Dove;
import fr.esaip.b2.poo.birds.Duck;
import fr.esaip.b2.poo.birds.ICanFly;
import fr.esaip.b2.poo.bo.Cercle;
import fr.esaip.b2.poo.bo.Figure;
import fr.esaip.b2.poo.bo.Rectangle;

public class App {
	
	public static void main( String[] args ) {
		
		ICanFly[] flyersTab = {new Duck( "Donald" ), new Dove( "For Peace" )};
		for ( ICanFly item : flyersTab ) {
			item.fly();
		}
		
		Figure[] tab = {new Cercle( "C1", "B", 5 ), new Cercle( "C1", "C", 15 ), new Rectangle( "R1", "R", 15, 20 )};
		
		for ( Figure item : tab ) {
			Cercle cercle = ( Cercle ) item;//Downcast avec risque d'exception ClassCastException si jamais item n'est pas du type attendu (Rectangle par ex)
			Figure f = cercle;
			System.out.println( "Pour la figure '" + item.getNom() + "': " );
			System.out.println( "la surface est : " + item.calculSurface() );
			System.out.println( "le périmètre est : " + item.calculPerimetre() );
		}
	}
}
